
<div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                 <li>
                                    <a href="admin_dash.php">Dashboard</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="chart.html">
                                <i class="fas fa-chart-bar"></i>Manage Users</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="fas fa-table"></i>Manage eCommerce</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="fas fa-map-marker"></i>Locale</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="fas fa-newspaper"></i>Reports</a>
                        </li>
                        <li>
                            <a href="form.html">
                                <i class="far fa-check-square"></i>System Components</a>
                        </li>
                       
                    </ul>
                </nav>
            </div>