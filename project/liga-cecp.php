<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

    <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>What is Community-Managed E-Commerce Platform or CMECP?</h2>
                        </div>
                        <p class="lead">&nbsp; &nbsp; &nbsp; &nbsp; The Community-Managed E-Commerce Platform or CMECP is a pool
                            of local resources and experiences that can be harnessed to alleviate poverty through project
                            innovations and business enterprise of E-Commerce approaches and strategies. It provides a
                            combination of social enterprise and traditional business models optimizing existing solutions,
                            capacities and good practices from among members, partners and communities.

                        </p>

                        <p class="lead">&nbsp; &nbsp; &nbsp; &nbsp; By employing the CMECP concepts, members will be able to
                            generate and sustain a new source of income. The approach is not only about alleviating poverty, but
                            also about recovering our pride as a Filipino people who can rise above and triumph over poor and
                            helpless situations by simply harnessing our strengths, talents, innovations, concepts and resources
                            to achieve something that can be sustained by the next generation of Filipinos. This platform will
                            explore and help discover new approaches and methodologies for this millennial generation toward
                            success.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h5>Two Major Strategies</h5>
                        </div>
                        <div class="row services text-center">
                            <div class="col-md">
                                <div class="box-simple">
                                    <div class="icon-outlined"><i class="fa fa-lightbulb-o"></i></div>
                                    <h3 class="h4">Entrepreneurs Innovations</h3>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="box-simple">
                                    <div class="icon-outlined"><i class="fa fa-wrench"></i></div>
                                    <h3 class="h4">Building E-Pipeline and Capacities</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>