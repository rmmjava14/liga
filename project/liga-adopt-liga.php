<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

    <div id="content">
    <div class="container">
      <div id="error-page" class="col-md-8 mx-auto text-center">
        <div class="box">
          <p class="text-center"><a href="../index2.html"><img src="../img/logo.png" alt="LiGa template"></a></p>
          <h3>We are sorry - this page is under construction</h3>
          <h4 class="text-muted">Error 404 - Page not found</h4>
          <p class="buttons"><a href="../index2.html" class="btn btn-template-outlined"><i class="fa fa-home"></i> Go to Homepage</a></p>
        </div>
      </div>
    </div>
  </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>