<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

  <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row mb-5">
                    <div class="col-md-12 mb-5">
                        <div class="heading text-center">
                            <h2>Disclosure Acknowledgement </h2>
                        </div>

                        <p class="lead"> The Likha at Galing E-Commerce Project Management Services (LiGa) is a
                            community of individuals who works together to create the potentials of ecommerce and
                            project development processes.
                        </p>

                        <p class="lead">The Pathfinder Accompaniment Platform Project Management System or APPMS,
                            however, is developed as a support mechanism to capacitate the members in managing the
                            projects and establishing their own e-commerce business model. </p>

                        <p class="lead"> We emphasize that the “Pathfinder Accompaniment Platform Project Management
                            System or APPMS” is not a get rich quick scheme but it will require a specific project or
                            purpose through ecommerce framework. The project should contribute sustainability addressing
                            the triple bottom line such as balancing economic, environment, and social perspective.
                            Thus, the Pathfinder APPMS under the LiGa platform is: </p>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="heading text-center">
                            <h2>How to become a member of LiGa Team?</h2>
                        </div>

                        <p class="lead"> The membership for LiGa community is at least 18 years old regardless of race,
                            sex, nationality, religious and or political beliefs. The processes and strategies on how to
                            get involved with our community should have to follow the different stages of managing their
                            project. The first move is to follow the “Lead Up Steps” and then need to complete the three
                            major phases of Pathfinder APPMS, namely; Startup Accelerator Phase, Do Up Move Up Phase,
                            and Development Phase.
                        </p>

                        <p class="lead">The LiGa Connect Host Team will practice a self-help learning approach within
                            the members to
                            discover strength and weaknesses for improvement of each members of the connect groups. It
                            will focus on membership development that will maximize the structure of commercial
                            undertakings to achieve social goals. </p>

                        <img src="../img/step.png" alt="">

                        <div class="text-center p-3"><a href="liga-package.php" class="btn btn-template-outlined">Be a Member Now</a></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>