<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

   <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="heading text-center">
                            <h2>What are the Benefits of LiGa Members? </h2>
                        </div>

                        <div class="row">
                            <div class="col">
                                <img src="../img/benefit-1.png" alt="" style="height: 600px;">
                            </div>
                            <div class="col">
                                <img src="../img/benefit-2.png" alt="" style="height: 600px;">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>