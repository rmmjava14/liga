<?php 
session_start();
//require("../model/config.php");
//require("../model/dbmysqli.php");
//$db = new unreal4u\dbmysqli();
//$db->throwQueryExceptions = true;
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<div id="all">
    <!-- Top bar-->
<?php
include("../navL.php");
?>
    <!-- Navbar End-->

    <div id="content">
        <div class="container">
          <div class="row">
    <div class="col-lg-6">
              <div class="box">
                <h2 class="text-uppercase">Account Info</h2>
              
                <form action="../controller/xInMemReg.php" method="post">
                    <div class="form-group">
                      <label for="name-login">First Name</label>
                      <input id="name-login" type="text" class="form-control" name="fname" required="required">
                    </div>
                    <div class="form-group">
                        <label for="name-login">Middle Name</label>
                        <input id="name-login" type="text" class="form-control" name="mname" required="required">
                      </div>
                      <div class="form-group">
                        <label for="name-login">Last Name</label>
                        <input id="name-login" type="text" class="form-control" name="lname" required="required">
                      </div>
                    <div class="form-group">
                      <label for="email-login">Email</label>
                      <input id="email-login" type="text" class="form-control" name="eMail" required="required">
                    </div>
                    <div class="form-group">
                      <label for="password-login">Password</label>
                      <input id="password-login" type="password" class="form-control" name="sekreto" required="required">
                    </div>
                    <input  type="hidden" class="form-control" name="uri" required="required" value="<?=$_GET['package']?>">
                    <div class="text-center">
                      <button type="submit" class="btn btn-template-outlined"><i class="fa fa-user"></i> Register</button>
                    </div>
                  </form>
              </div>
            </div>
            <div class="col-lg-6">
                <div class="box">
                <?php
                if($_GET['package']=="222"){
                ?>
                    
                <div class="package">
                            <div class="package-header bg-primary color-white">
                                <h5>Premium</h5>
                            </div>
                            <div class="price-container d-flex align-items-end justify-content-center">
                                <h4 class="h1"><span class="currency">Php </span>4,850</h4>
                            </div>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check"></i>Premium Content</li>
                                <li><i class="fa fa-times"></i>Premium Content</li>
                                <li><i class="fa fa-times"></i>Premium Content</li>
                            </ul>
                            
                            <br>
                            <img src="../img/liga/premium.png" alt="">
                        </div>
                <?php
                }else if($_GET['package']=="333"){
                ?>
                <div class="package">
                                <div class="package-header bg-warning color-white">
                                    <div class="content">
                                        <h5>Century</h5>
                                      
                                    </div>
                                </div>
                                <div class="price-container d-flex align-items-end justify-content-center">
                                    <h4 class="h1"><span class="currency">Php </span>9,850</h4>
                                </div>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i>Century Content</li>
                                    <li><i class="fa fa-check"></i>Century Content</li>
                                    <li><i class="fa fa-times"></i>Century Content</li>
                                </ul>
                                <br>
                                <img src="../img/liga/century.png" alt="">
                            </div>
                <?php
                }else if($_GET['package']=="444"){
                ?>
                    <div class="package">
                            <div class="package-header bg-danger color-white">
                                <h5 >Millennium</h5>
                            </div>
                            <div class="price-container d-flex align-items-end justify-content-center">
                                <h4 class="h1"><span class="currency">Php </span>19,850</h4>
                            </div>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check"></i>Millennium Content</li>
                                <li><i class="fa fa-check"></i>Millennium Content</li>
                                <li><i class="fa fa-check"></i>Millennium Content</li>
                            </ul>
                            <br>
                            <img src="../img/liga/millennium.png" alt="">
                        </div>
                <?php  
                }
                ?>

              </div>
                 
               
              </div>
          </div>
        </div>
      </div>

    <!-- GET IT-->
    
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>