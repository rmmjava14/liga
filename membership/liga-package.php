<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

  
  <div id="content">
        <div class="container">
            <section class="bar pb-0">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>LiGa packages</h2>
                        </div>
                    </div>
                </div>
            </section>
            <section class="bar">
                <div class="row packages">
                    <div class="col-md-4">
                        <div class="package">
                            <div class="package-header bg-primary color-white">
                                <h5>Premium</h5>
                            </div>
                            <div class="price-container d-flex align-items-end justify-content-center">
                                <h4 class="h1"><span class="currency">Php </span>4,850</h4>
                            </div>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check"></i>Premium Content</li>
                                <li><i class="fa fa-times"></i>Premium Content</li>
                                <li><i class="fa fa-times"></i>Premium Content</li>
                            </ul><a href="liga-reg.php?package=222" class="btn btn-template-outlined mb-3">Sign Up</a>
                            <br>
                            <img src="../img/liga/premium.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="best-value">
                            <div class="package">
                                <div class="package-header bg-warning color-white">
                                    <div class="content">
                                        <h5>Century</h5>
                                        <div class="meta-text">Best Value</div>
                                    </div>
                                </div>
                                <div class="price-container d-flex align-items-end justify-content-center">
                                    <h4 class="h1"><span class="currency">Php </span>9,850</h4>
                                </div>
                                <ul class="list-unstyled">
                                    <li><i class="fa fa-check"></i>Century Content</li>
                                    <li><i class="fa fa-check"></i>Century Content</li>
                                    <li><i class="fa fa-times"></i>Century Content</li>
                                </ul><a href="liga-reg.php?package=333" class="btn btn-template-outlined mb-3">Sign Up</a>
                                <br>
                                <img src="../img/liga/century.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="package">
                            <div class="package-header bg-danger color-white">
                                <h5 >Millennium</h5>
                            </div>
                            <div class="price-container d-flex align-items-end justify-content-center">
                                <h4 class="h1"><span class="currency">Php </span>19,850</h4>
                            </div>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-check"></i>Millennium Content</li>
                                <li><i class="fa fa-check"></i>Millennium Content</li>
                                <li><i class="fa fa-check"></i>Millennium Content</li>
                            </ul><a href="liga-reg.php?package=444" class="btn btn-template-outlined mb-3">Sign Up</a>
                            <br>
                            <img src="../img/liga/millennium.png" alt="">
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="box">
                        <h2 class="text-uppercase">As a Guest</h2>
                        <p class="lead">Want to buy products and services?</p>
                        <p class="text-muted">Signing-up as a guest allows you to shop at LiGa without a membership.</p>

                        <div class="text-center">
                            <button class="btn btn-template-outlined">Sign Up</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                   <div class="box">
                       <h2 class="text-uppercase">As a LiGa Host Team </h2>
                       <p>With registration with us new world of fashion, fantastic discounts and much more opens to you!</p>
                       <p class="text-muted">If you have any questions, please feel free to <a href="contact.html">contact us</a>, our customer service center is working for you 24/7.</p>

                       <div class="text-center">
                           <button class="btn btn-template-outlined">Learn More</button>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.html" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>