<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

    <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>What are the Qualifications of Membership? </h2>
                        </div>

                        <p class="lead">1. The main goal of a member is not only to earn but to create a positive impact
                            on society by using that project for the welfare of their families, communities and the
                            environment. </p>

                        <p class="lead">2. Embracing the triple bottom line (TBL)—balancing economic, environment and
                            social success. </p>

                        <p class="lead">3 .Willing wholeheartedly to implement and monitor your project. </p>

                        <p class="lead">4. Willing to help others. </p>

                        <p class="lead">5. Willing to take part to “Alleviate Poverty”. </p>

                        <p class="lead">6. Teachable and willing to learn the Pathfinder APPMS </p>

                        <p class="lead">7. Willing to pay the Pathfinder Accelerator fee. </p>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>