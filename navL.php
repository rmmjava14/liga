
<div id="all">
    <!-- Top bar-->
    <div class="top-bar">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-md-6 d-md-block d-none">
                    <img src="<?=$_SESSION["liga_path"]?>img/quote.png" alt="Quote" style="height: 18px;">
                </div>
                <div class="col-md-6">
                    <div class="d-flex justify-content-md-end justify-content-between">
                        <ul class="list-inline contact-info d-block d-md-none">

                            <li class="list-inline-item"><a href="#" data-toggle="modal" data-target="#login-modal" class="login-btn">Sign In</a></li>
                        </ul>
                        <div class="login">
                            <a href="#" data-toggle="modal" data-target="#login-modal" class="login-btn">
                                <span class="d-none d-md-inline-block">Sign In</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top bar end-->
    <!-- Login Modal-->
    <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modalLabel" aria-hidden="true"
         class="modal fade">
        <div role="document" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="login-modalLabel" class="modal-title">Member Login</h4>
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span
                            aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form action="<?=$_SESSION["liga_path"]?>controller/login.php" method="POST">
                        <div class="form-group">
                            <input id="email_modal" type="email" placeholder="email" class="form-control" name="UserName">
                        </div>
                        <div class="form-group">
                            <input id="password_modal" type="password" placeholder="password" class="form-control" name="PassWord">
                        </div>
                        <p class="text-center">
                            <button class="btn btn-template-outlined"><i class="fa fa-sign-in"></i> Log in</button>
                        </p>
                    </form>
                    <p class="text-center text-muted">Not registered yet? <a href="<?=$_SESSION["liga_path"]?>membership/liga-package.php"><strong>Register now</strong></a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Login modal end-->
    <!-- Navbar Start-->
    <header class="nav-holder make-sticky">
        <div id="navbar" role="navigation" class="navbar navbar-expand-lg">
            <div class="container "><a href="<?=$_SESSION["liga_path"]?>index.php" class="navbar-brand home"><img src="<?=$_SESSION["liga_path"]?>img/logo.png" alt="LiGa logo" class="d-none d-md-inline-block"><img src="img/logo-small.png" alt="Universal logo" class="d-inline-block d-md-none"><span class="sr-only">LiGa - go to homepage</span></a>
                <button type="button" data-toggle="collapse" data-target="#navigation" class="navbar-toggler "><span class="sr-only">Toggle navigation</span><i class="fa fa-align-justify"></i></button>
                <div id="navigation" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item dropdown "><a href="<?=$_SESSION["liga_path"]?>index.php">Home</a>
                        <li class="nav-item dropdown "><a href="javascript: void(0)" data-toggle="dropdown"
                                                          class="dropdown-toggle">About Us <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>about-us/liga-about.php" class="nav-link">What is LiGa</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>about-us/liga-core-values.php" class="nav-link">Our Core Values</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>about-us/liga-business-str.php" class="nav-link">Our Business Strength</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>about-us/liga-host-team.php" class="nav-link">The Connect Host Team</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>about-us/liga-team.php" class="nav-link">The Country Host Team</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown "><a href="javascript: void(0)" data-toggle="dropdown"
                                                          class="dropdown-toggle">Projects <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>project/liga-cecp.php" class="nav-link">Community-Managed E-Commerce Platform</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>platform/platform-liga.php" class="nav-link">Adopt LiGa Project Program</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown "><a href="javascript: void(0)" data-toggle="dropdown"
                                                          class="dropdown-toggle">Marketing Plan <b
                                class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>marketing-plan/liga-earning.php" class="nav-link">Ways of Earnings</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>marketing-plan/liga-payment-plan.php" class="nav-link">Payment Plan</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown "><a href="<?=$_SESSION["liga_path"]?>blog-event/blog-event.php">Blog & Upcoming Event</a>
                        </li>
                        <li class="nav-item dropdown "><a href="javascript: void(0)" data-toggle="dropdown"
                                                          class="dropdown-toggle">Membership <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>membership/liga-how-to.php" class="nav-link">How to Become a Member of LiGa</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>membership/liga-qualification.php" class="nav-link">Qualifications of Membership</a></li>
                                <li class="dropdown-item"><a href="<?=$_SESSION["liga_path"]?>membership/liga-benefit.php" class="nav-link">What are the Benefits of LiGa Members</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id="search" class="collapse clearfix">
                    <form role="search" class="navbar-form">
                        <div class="input-group">
                            <input type="text" placeholder="Search" class="form-control"><span class="input-group-btn">
                    <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>