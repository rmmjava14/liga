<?php
session_start();
if ($_SESSION['logged'] == '1') {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        require("../model/config.php");
        require("../model/dbmysqli.php");
        $db = new unreal4u\dbmysqli();
        $db->throwQueryExceptions = true;
        $db->query("SET SESSION sql_mode = ''");
        $pID = strtoupper(trim($_POST['pID']));
        $pName = strtoupper(trim($_POST['pName']));
        $pDesc = strtoupper(trim($_POST['pDesc']));
        $pCost = strtoupper(trim($_POST['pCost']));
        $curqty = "";
        $diff = "";
        $newqty = strtoupper(trim($_POST['pQty']));
        $oldqty = strtoupper(trim($_POST['pQty2']));
        if($newqty == $oldqty){
            $curqty = $oldqty;
    $kk =$db->query("SELECT `counterID`,`deliveryDate`, `itemCode`, `added`, `atCost` FROM `z_item_counter` WHERE `itemCode` = ? AND `deliveryDate` = (SELECT  MAX(`deliveryDate`) FROM z_item_counter WHERE itemCode = ?)", $pID, $pID);
        if ($db->num_rows > 0) {
            foreach($kk as $kk){
                      
                      $cID = $kk['counterID'];
                  }
        }
        $db->begin_transaction();
        $db->query("UPDATE `z_item_counter` SET `atCost`= ? WHERE `counterID`= ?", $pCost, $cID);
        $db->end_transaction();

        }else{
            $curqty = $newqty;
            $diff = $newqty - $oldqty;
            $kk =$db->query("SELECT `counterID`,`deliveryDate`, `itemCode`, `added`, `atCost` FROM `z_item_counter` WHERE `itemCode` = ? AND `deliveryDate` = (SELECT  MAX(`deliveryDate`) FROM z_item_counter WHERE itemCode = ?)", $pID, $pID);
        if ($db->num_rows > 0) {
            foreach($kk as $kk){
                      $added = $kk['added'];
                      $cID = $kk['counterID'];
                  }
        }
        $finQty = $added + $diff;
        $db->begin_transaction();
        $db->query("UPDATE `z_item_counter` SET `added`=  ?, `atCost`= ? WHERE `counterID`= ?", $finQty, $pCost, $cID);
        $db->end_transaction();
        }
        
        
        
        $pClass = strtoupper(trim($_POST['pClass']));



        $pClassID = strtoupper(trim($_POST['pClassID']));
        $pCID = strtoupper(trim($_POST['pCID']));
        $truepClass="";
        if($pCID){
            $truepClass=$pCID;
            switch ($pCID) {
            case ($pCID == 201850030):
                        # foodies...
                  $f =$db->query("SELECT `fID`, `foodType`, `itemCode` FROM `z_food_type_list` WHERE `itemCode` = ?", $pID);
                  if ($db->num_rows > 0) {
                    $sql = "UPDATE `z_food_type_list` SET `foodType`= ? WHERE `itemCode`= ?";
                  }else{
                    $sql = "INSERT INTO `z_food_type_list`(`foodType`, `itemCode`) VALUES(?,?)";
                    $db->query("DELETE FROM `z_med_type_list` WHERE `itemCode`= ?", $pID);
                    $db->query("DELETE FROM `z_supplies_type_list` WHERE `itemCode`= ?", $pID);
                  }
                  
              break;
              case ($pCID == 201818030):
                        # xtalmeth...  
           $f =$db->query("SELECT `mID`, `medType`, `itemCode` FROM `z_med_type_list` WHERE `itemCode` = ?", $pID);
                  if ($db->num_rows > 0) {
                    $sql = "UPDATE `z_med_type_list` SET `medType`= ? WHERE `itemCode`= ?";
               }else{
                     $sql = "INSERT INTO `z_med_type_list`(`medType`, `itemCode`) VALUES(?,?)";
                     $db->query("DELETE FROM `z_food_type_list` WHERE `itemCode`= ?", $pID);
                     $db->query("DELETE FROM `z_supplies_type_list` WHERE `itemCode`= ?", $pID);
               }
                 break;
                case ($pCID == 201830205):
                        # foodies...
                  $f =$db->query("SELECT `stID`, `supplyType`, `itemCode` FROM `z_supplies_type_list` WHERE `itemCode` = ?", $pID);
                  if ($db->num_rows > 0) {
                    $sql = "UPDATE `z_supplies_type_list` SET `supplyType`= ? WHERE `itemCode`= ?";
                  }else{
                    $sql = "INSERT INTO `z_supplies_type_list`(`supplyType`, `itemCode`) VALUES(?,?)";
                    $db->query("DELETE FROM `z_med_type_list` WHERE `itemCode`= ?", $pID);
                    $db->query("DELETE FROM `z_food_type_list` WHERE `itemCode`= ?", $pID);
                  }
                  
              break;
                      default:
                        # code...
                        break;
        }

        }else{
            $truepClass=$pClassID;
            switch ($pClass) {
            case 'FOOD':
                        # foodies...
                  $sql = "UPDATE `z_food_type_list` SET `foodType`= ? WHERE `itemCode`= ?";
              break;
              case 'MEDICINE':
                        # xtalmeth...              
                    $sql = "UPDATE `z_med_type_list` SET `medType`= ? WHERE `itemCode`= ?";
               
                        break;
             case 'SUPPLIES':
                        # xtalmeth...              
                    $sql = "UPDATE `z_supplies_type_list` SET `supplyType`= ? WHERE `itemCode`= ?";
               
                        break;
                      default:
                        # code...
                        break;
        }
        }

        $pTypeID = strtoupper(trim($_POST['pTypeID']));
        $pTID = strtoupper(trim($_POST['pTID']));
        $truePType="";
        if($pTID){
            $truePType=$pTID;
        }else{
            $truePType=$pTypeID;
        }

        

       
        
            try {                
               
                //add to record
                $db->begin_transaction();
                $db->query("UPDATE `z_items` SET `itemName`= ?,`itemDesc`=?,`itemQty`=? WHERE `itemCode`= ?", $pName, $pDesc, $curqty, $pID);
                $db->query("UPDATE `z_item_material_rel` SET `matType`=  ? WHERE `itemCode`= ?", $truepClass, $pID);
                $db->query($sql, $truePType, $pID);
                $db->end_transaction();
                $t=1;               
                $msg = 'Successfully updated';
                header("Location: ../view/items.php?m=" . $msg . "&ty=".$t);
          
             } catch (unreal4u\queryException $e) {
                print('We have captured a query exception!');
                var_dump($e->getMessage());
                print_r($_POST);
            }
        
    } else {
        header("Location: aDashboard.php");
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
?>
