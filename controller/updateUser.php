<?php 
session_start();
date_default_timezone_set('Asia/Taipei');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
require("../model/config.php");
        require("../model/dbmysqli.php");
require("Classes/GUMP/gump.class.php");
include("sterile.php");
$db   = new unreal4u\dbmysqli();
$db->throwQueryExceptions = true;
$db->query("SET SESSION sql_mode = ''");
$gump = new GUMP();
$request = $gump->sanitize($_POST);
$updatePassword = false;
if ($request["pword"] != ""){
	$updatePassword = true;
}

if(isset($request["gender"])){
	$kasarian = $request["gender"];
}else{
	$kasarian = $request["oldgen"];
}

if($request["role"] == ""){
	$role = $request["olt"];
	
}else{
	$role = $request["role"];
}


try{   
  $sys_users = $db->query("SELECT `uID`, `UserName` FROM `users` WHERE `UserName` = ?  AND  `uID` <> ?",$request["uname"],$request["uid"]);     
                  }catch (unreal4u\queryException $e) {
                    print('We have captured a query exception!');
                    var_dump($e->getMessage());
                  }/////try
                  if ($db->num_rows > 0) {
                  	$msg="Username taken. Try another.";
                  	$typer = 2;
                  	header("Location: ../view/userMngtEdit.php?m=" . $msg . "&ty=".$ty."&u=".$request["uid"]);
}else{

try {
	
	$db->begin_transaction();
	if ($updatePassword){ 
		$db->query("UPDATE `users` SET `UserName`= ?,`fName`= ?,`mName`= ?,`sName`= ?,`PassWord`= ?,`eMail`= ? WHERE `uID` = ?",
				$request["uname"], $request["fn"], $request["mn"], $request["ln"], $request["pword"], $request["eMail"], $request["uid"]
		);
	}else{
		$db->query("UPDATE `users` SET `UserName`= ?,`fName`= ?,`mName`= ?,`sName`= ?,`eMail`= ? WHERE `uID` = ?",
				$request["uname"], $request["fn"], $request["mn"], $request["ln"], $request["eMail"], $request["uid"]
		);
	}

	$db->query("UPDATE `profiles` SET `address`= ?, `phone`= ?, `gender`= ? WHERE `pID` = ?",
			$request["adr"], $request["cNum"], $kasarian, $request["uid"]
	);

$db->query("UPDATE `user_type_list` SET `uTypeIDs`= ? WHERE `uIDs`= ?",
			$role, $request["uid"]
	);

	$db->end_transaction();
	
	$msg = "Profile updated";
	$ty = 1;
	header("Location: ../view/userMngtEdit.php?m=" . $msg . "&ty=".$ty."&u=".$request["uid"]);
} catch (unreal4u\queryException $e) {
	print('We have captured a query exception! '. $e->getMessage());
	die();
}
}

}

?>





















