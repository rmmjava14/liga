<?php
session_start();
if ($_SESSION['logged'] == '1') {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        require("../model/config.php");
        require("../model/dbmysqli.php");
        $db = new unreal4u\dbmysqli();
        $db->throwQueryExceptions = true;
        $db->query("SET SESSION sql_mode = ''");
        
        $iITem = strtoupper(trim($_GET['sw']));
		$ani = strtoupper(trim($_GET['an']));
        $tID = strtoupper(trim($_GET['tID']));
        if ($iITem) {
            try {                
             
                //add to record
                $db->begin_transaction();
                $db->query("DELETE FROM `z_product_sales` WHERE `salesID` = ?", $iITem);
                $db->query("UPDATE `x_animal_main_info` SET `aStatus`= ? WHERE `animalID`= ?", 1,$ani);
                $db->end_transaction();
                $t=1;               
                $msg = 'Item Removed';
                header("Location: ../view/Cart.php?tID=". $tID ."&m=" . $msg . "&ty=".$t."");
           
             } catch (unreal4u\queryException $e) {
                print('We have captured a query exception!');
                var_dump($e->getMessage());
                print_r($_POST);
            }
        } else{
            $t=3;
            $msg = "No Item Selected";
            header("Location: ../view/Cart.php?tID=". $tID ."&m=" . $msg . "&ty=".$t."");
        }
    } else {
        header("Location: aDashboard.php");
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
?>
