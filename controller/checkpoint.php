<?php
date_default_timezone_set("Asia/Taipei");
function _alcohol_input($r)
    {
        $tr = trim($r);
        $tr = filter_var($tr, FILTER_SANITIZE_STRING);
        return $tr;
    }
function _verify($username,$password,$base){
			$username=_alcohol_input( $username );
			$password=_alcohol_input( $password );			
			$likeun = $username;
			$likepw =  $password ;
			$uStat = 1;
			$db=$base;
	$when = date("Y-m-d");
    $oras = date("h:i");  		
	$db->begin_transaction();
	$rs = $db->query('SELECT * FROM users u LEFT JOIN user_type_list l ON l.uIDs=u.uID LEFT JOIN user_types t ON t.typeID=l.uTypeIDs WHERE u.UserName = ? AND u.PassWord = ? AND u.uStatus=? LIMIT 1',$likeun,$likepw,$uStat);
			if($db->num_rows == 0){
			return 0;	
			}
			elseif($db->num_rows > 0){		
				foreach($rs AS $crd) {
  							$uTypeID=$crd['utID'];
								$uAno=$crd['typeDesc'];
									$iam=$crd['fName'].' '.$crd['sName'];
										$usergen=$crd['UserName']; 
											$hashedgen=$crd['PassWord'];
												$mukha=$crd['pImg'];	
													$uID=$crd['uID'];}
														session_regenerate_id();
														$_SESSION['logged'] = TRUE; 
															$_SESSION['userid'] = $uID;
																$_SESSION['UserName'] = $username;
																	$_SESSION['PassWord'] = $password;
																		$_SESSION['logtype'] = $uAno;
																			$_SESSION['human'] = $iam;											 
																			$_SESSION['img'] = $mukha;
																		$_SESSION['fName'] = $crd['fName'];
																	$_SESSION['mName'] = $crd['mName'];
																$_SESSION['sName'] = $crd['sName'];
															session_write_close();
														setcookie("UserName",$username);
													setcookie("PassWord",$password);
												setcookie('userid', $uID);
											setcookie('typeofuser', $uAno);
									   $act = "Logged in to the system";	
                                       $db->query("INSERT INTO `users_activity`(`usersID`, `actDesc`, `actDate`, `actTime`) VALUES(?,?,?,?)",$uID, $act,$when, $oras);
                                    
										$db->end_transaction();
									return TRUE;								
							}else{ return FALSE;}
						}

function _verifymember($username,$password,$base){
							$username=_alcohol_input( $username );
							$password=_alcohol_input( $password );			
							$likeun = $username;
							$likepw =  $password ;
							$uStat = 1;
							$db=$base;
					$when = date("Y-m-d");
					$oras = date("h:i");  		
					$db->begin_transaction();
					$rs = $db->query('SELECT * FROM users u LEFT JOIN user_type_list l ON l.uIDs=u.uID LEFT JOIN user_types t ON t.typeID=l.uTypeIDs WHERE u.eMail = ? AND u.PassWord = ? AND u.uStatus=? LIMIT 1',$likeun,$likepw,$uStat);
							if($db->num_rows == 0){
							return 0;	
							}
							elseif($db->num_rows > 0){		
								foreach($rs AS $crd) {
											  $uTypeID=$crd['utID'];
												$uAno=$crd['typeDesc'];
													$iam=$crd['fName'].' '.$crd['sName'];
														$usergen=$crd['UserName']; 
															$hashedgen=$crd['PassWord'];
																$mukha=$crd['pImg'];	
																	$uID=$crd['uID'];}
																		session_regenerate_id();
																		$_SESSION['logged'] = TRUE; 
																			$_SESSION['userid'] = $uID;
																				$_SESSION['UserName'] = $username;
																					$_SESSION['PassWord'] = $password;
																						$_SESSION['logtype'] = $uAno;
																							$_SESSION['human'] = $iam;											 
																							$_SESSION['img'] = $mukha;
																						$_SESSION['fName'] = $crd['fName'];
																					$_SESSION['mName'] = $crd['mName'];
																				$_SESSION['sName'] = $crd['sName'];
																			session_write_close();
																		setcookie("UserName",$username);
																	setcookie("PassWord",$password);
																setcookie('userid', $uID);
															setcookie('typeofuser', $uAno);
													   $act = "Logged in to the system";	
													//   $db->query("INSERT INTO `users_activity`(`usersID`, `actDesc`, `actDate`, `actTime`) VALUES(?,?,?,?)",$uID, $act,$when, $oras);
													
														$db->end_transaction();
													return TRUE;								
											}else{ return FALSE;}
										}

function _wentAway() 
{	ob_start();
	session_start();
	$_SESSION['logged'] = FALSE;
	$_SESSION['UserName'] = '';
	$_SESSION['PassWord'] = '';
	$_SESSION = array();
	setcookie('UserName', '', false);
	setcookie('PassWord', '', false);
	setcookie('userid', '', false);
	setcookie('typeofuser', '', false);
	ob_flush();
	session_unset();
	session_destroy();
	header('Location:/liga/');
	die();
}

?>