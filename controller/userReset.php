<?php
session_start();
if ($_SESSION['logged'] == '1') {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        require("../model/config.php");
        require("../model/dbmysqli.php");
        $db = new unreal4u\dbmysqli();
        $db->throwQueryExceptions = true;
        $db->query("SET SESSION sql_mode = ''");
        $uID = strtoupper(trim($_GET['ui']));        
        //$shortName = strtoupper(trim($_POST['shortName']));

        if ($uID != '') {
            $used = $db->query("SELECT `uID`, `orUserName`, `orPassword`, `resCount` FROM `reusers` WHERE `uID` = ?", $uID);
            if($db->num_rows > 0){
                foreach($used as $used){
                        $ou = $used['orUserName'];
                         $op = $used['orPassword'];
                }

                try {
                //add to record
                $db->begin_transaction();
                $db->query("UPDATE `users` SET `UserName`=?, `PassWord`=? WHERE `uID`=?",$ou,$op,$uID);
                $db->end_transaction();
                $t=1;
                $msg = "User login details reset";
                header("Location: ../view/userManage.php?m=".$msg."&ty=".$t."#breeder");
            } catch (unreal4u\queryException $e) {
                print('We have captured a query exception!');
                var_dump($e->getMessage());
                print_r($_POST);
            }

                
            }else{
            
                $t=2;
                $msg = "Something went wrong. Please Try again";
                header("Location: ../view/userManage.php?m=".$msg."&ty=".$t."#breeder");
        }
        }else {
            $t=3;
            $msg = "user ID missing";
            header("Location: ../view/userManage.php?m=".$msg."&ty=".$t."#breeder");
        }
    } else {
        header("Location: aDashboard.php");
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
?>
