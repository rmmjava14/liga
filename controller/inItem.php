<?php
session_start();
if ($_SESSION['logged'] == '1') {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        require("../model/config.php");
        require("../model/dbmysqli.php");
        $uID = $_SESSION['userid'];
        $act = "Created a New Animal Record";
        $when = date("Y-m-d");
        $oras = date("h:i");     
        $iCode = strtoupper(trim($_POST['iCode']));
        $iName = strtoupper(trim($_POST['iName']));
        $iDesc = strtoupper(trim($_POST['iDesc']));
        $iQty = strtoupper(trim($_POST['iQty']));
        $iClass = strtoupper(trim($_POST['iClass']));
        $iType = strtoupper(trim($_POST['iType']));
        $iCost = strtoupper(trim($_POST['iCost']));
        $iSup = strtoupper(trim($_POST['iSup']));
        $iVoice = strtoupper(trim($_POST['iVoice']));
        $iDate = strtoupper(trim($_POST['iDate']));        
        $actioneer = strtoupper(trim($_POST['actioneer']));
        $sql = "";
        $db = new unreal4u\dbmysqli();
        $db->throwQueryExceptions = true;
        $db->query("SET SESSION sql_mode = ''");
          $matty =$db->query("SELECT `matID`, `matDesc` FROM `z_materials_type` WHERE `matID` = ?", $iClass);
              if ($db->num_rows > 0) {
                  foreach($matty as $mx){
                      $what = $mx['matDesc'];
                  }
              }
                    switch ($what) {
                      case 'FOOD':
                        # foodies...
                          $sql = "INSERT INTO `z_food_type_list`(`foodType`, `itemCode`) VALUES(?,?)";
                        break;
                      case 'MEDICINE':
                        # xtalmeth...
                          $sql = "INSERT INTO `z_med_type_list`(`medType`, `itemCode`) VALUES(?,?)";
                        break;
                      case 'SUPPLIES':
                        # xtalmeth...
                          $sql = "INSERT INTO `z_supplies_type_list`(`supplyType`, `itemCode`) VALUES(?,?)";
                        break;
                      default:
                        # code...
                        break;
                    }
        
 /////////////////////////////////////////////////////////////////
    switch ($actioneer) {
                       case 'SAVER':

                          # save...
                $db->query("SELECT `itemCode`, `itemName`, `itemDesc` FROM `z_items` WHERE `itemCode` = ? OR (`itemName` = ? AND `itemDesc` = ?)", $iCode,$iName,$iDesc);
            if ($db->num_rows > 0) {
                $msg = 'Item exists';
                $ty = 0;
                header("Location: ../view/items.php?m=" . $msg . "&ty=".$ty);
            }else{
            if($iCode && $iName && $iDesc && $iQty && $iClass && $iType && $iSup && $iDate){
              $act = "Created a New Item - ".$iName." Record";
            try {
                //add to record
                $db->begin_transaction();
                $db->query("INSERT INTO `z_items`(`itemCode`, `itemName`, `itemDesc`, `itemQty`) VALUES(?,?,?,?)", $iCode,$iName,$iDesc,$iQty);
                $db->query("INSERT INTO `z_item_counter`(`itemCode`, `added`, `atCost`, `supplier`, `deliveryDate`, `invoiceNumber`) VALUES(?,?,?,?,?,?)", $iCode,$iQty,$iCost,$iSup,$iDate,$iVoice);
                $db->query("INSERT INTO `z_item_material_rel`(`matType`, `itemCode`) VALUES(?,?)", $iClass,$iCode);
                $db->query("INSERT INTO `users_activity`(`usersID`, `actDesc`, `actDate`, `actTime`) VALUES(?,?,?,?)",$uID, $act,$when, $oras);
                $db->query($sql, $iType,$iCode);
                $db->end_transaction();
                $msg = "Successfully Created New Item ".$iName." - ".$iDesc."";
                $ty = 1;
                header("Location: ../view/items.php?m=" . $msg . "&ty=".$ty);
            } catch (unreal4u\queryException $e) {
                print('We have captured a query exception!');
                var_dump($e->getMessage());
                print_r($_POST);
            }
        } else {
            $ty = 2;
            $msg = 'You didn not enter a Description';
           header("Location: ../view/items.php?m=" . $msg . "&ty=".$ty);
        }
      }
      ///////////////////////////new rec////////////////////////
                   break;
                  /////////END OF SAVER/////////////////////////////////////////////////////////////////////

                       case 'UPDATER':
                           # update...
                           $iCode = strtoupper(trim($_POST['iCode']));
                           if($typeDesc){
                            $act = "Updated Material Type - ".$typeDesc." Record";
                                try {
                                    //add to record
                                    $db->begin_transaction();
                                    $db->query("UPDATE `z_food_type`  
                                                    SET `typeDesc`=?                                                        
                                                         WHERE `typeCode`= ?", $typeDesc,$sID);
                                    $db->query("INSERT INTO `users_activity`(`usersID`, `actDesc`, `actDate`, `actTime`) VALUES(?,?,?,?)",$uID, $act,$when, $oras);
                                    $db->end_transaction();
                                    $msg = "Details for Materials Type ".$typeDesc." Successfully Updated";
                                    $ty = 1;
                                    header("Location: ../view/items.php?m=" . $msg . "&ty=".$ty);
                                } catch (unreal4u\queryException $e) {
                                    print('We have captured a query exception!');
                                    var_dump($e->getMessage());
                                    print_r($_POST);
                                }
                            } else {
                                $ty = 2;
                                $msg = 'Oops! Must be a missing input. Try Again';
                               header("Location: ../view/items.php?m=" . $msg . "&ty=".$ty);
                            }

                           break;
                       
                       default:
                           # none...
                           break;
                   }               
////////////////SWITCHING //////////////////////
        
        
       
    } else {
        header("Location: aDashboard.php");
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
?>
