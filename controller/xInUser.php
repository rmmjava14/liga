<?php 
session_start();
date_default_timezone_set('Asia/Taipei');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
require("../model/config.php");
require("../model/dbmysqli.php");
require("Classes/GUMP/gump.class.php");
$db   = new unreal4u\dbmysqli();
$db->throwQueryExceptions = true;
$db->query("SET SESSION sql_mode = ''");
$gump = new GUMP();
$_POST = $gump->sanitize($_POST);
//determine what action must be done
$_action = $_POST["_action"];
switch ($_action) {
	case 'save':
		$request = $_POST;
		$validated_data = $gump->run($_POST);
		////////////////////////////////////////////
				try{   
                    $sys_users = $db->query("SELECT `fName`, `mName`, `sName`, `eMail` FROM `users` WHERE (`fName` = ? AND `sName` = ?) OR `eMail` = ?",$request["firstName"],$request["lastName"],$request["email"]);     
                  }catch (unreal4u\queryException $e) {
                    print('We have captured a query exception!');
                    var_dump($e->getMessage());
                  }/////try
                  if ($db->num_rows > 0) {
                  	$msg="User or eMail already on Record. Try another.";
                  	$typer = 2;
                  	header("location: ../view/userCreate.php?m=".$msg."&ty=".$typer);
        }else{
		////////////////////////////////////////////////
		$id = date('m') . date('Y') . mt_rand(1111,9999);		
		/******* no random char gen *************/
		$genuser=$request["firstName"].".".$request["lastName"].".SIMS";	 
		$genpasswd = $request["lastName"]."5678";
		/******* no random char gen *************/
		$pImg = "../view/images/user.png";
		
		
		try {
			$db->begin_transaction();
			$db->query("INSERT INTO `users`(`uID`, `UserName`, `fName`, `mName`, `sName`, `PassWord`, `eMail`, `pImg`, `uStatus`) VALUES (?,?,?,?,?,?,?,?,?)",
					$id, $genuser, $request["firstName"], $request["middleName"], $request["lastName"], $genpasswd, $request["email"],$pImg, 1
				);
			$db->query("INSERT INTO `profiles`(`pID`, `address`, `phone`, `gender`) VALUES (?,?,?,?)",
					$id, $request["address"],$request["contact"], $request["gender"]
				);
			$db->query("INSERT INTO `user_type_list`(`uTypeIDs`, `uIDs`) VALUES (?,?)",  $request["role"], $id);

			$db->query("INSERT INTO `reusers`(`uID`, `orUserName`, `orPassword`, `resCount`) VALUES (?,?,?,?)",  $id, $genuser,$genpasswd, 0);			
		 
			$db->end_transaction();
			$msg="Successfully created account for ".$request["firstName"]." ".$request["lastName"]."";
			$typer = 1;
			switch ($_SESSION['logtype']) {
			case "Administrator":			
            header("location: ../view/userCreate.php?m=".$msg."&ty=".$typer);
			break;
			case "Manager":
			header("location: ../view/userCreate.php?m=".$msg."&ty=".$typer);
			break;
			case "Encoder":
			header("location: ../view/userCreate.php?m=".$msg."&ty=".$typer);
			break;
			default:

			break;
					}
		}catch (unreal4u\queryException $e) {
			print('We have captured a query exception! '. $e->getMessage());
		}	
		}
		
		break;	
	
	case 'update':		
		break;
	//set status to isActive true
	case 'activate':
		$request = $_POST;
		try {
			$db->query("UPDATE `users` SET `uStatus`= 1 WHERE `uID` = ?", $request["user"]);
		} catch (unreal4u\queryException $e) {
			print('We have captured a query exception! '. $e->getMessage());
		}
		$msg="Successfully Change Status";
		$typer = 1;
		header("location: ../view/userManage.php?m=".$msg."&ty=".$typer);
		break;
	//set status to isActive false
	case 'disable':
		$request = $_POST;
		try {
			$db->query("UPDATE `users` SET `uStatus`= 0 WHERE `uID` = ?", $request["user"]);
		} catch (unreal4u\queryException $e) {
			print('We have captured a query exception! '. $e->getMessage());
		}
		header("location: UserMngt.users.php?m=success");
		break;
	default:
		# code...
		break;
}

}
