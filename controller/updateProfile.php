<?php 
session_start();
date_default_timezone_set('Asia/Taipei');
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
require("../model/config.php");
        require("../model/dbmysqli.php");
require("Classes/GUMP/gump.class.php");
include("sterile.php");
$db   = new unreal4u\dbmysqli();
$db->throwQueryExceptions = true;
$db->query("SET SESSION sql_mode = ''");
$gump = new GUMP();
$request = $gump->sanitize($_POST);
$updatePassword = false;
if ($request["pword"] != ""){
	$updatePassword = true;
}

$uID = $_SESSION['userid'];
        $when = date("Y-m-d");
        $oras = date("h:i"); 
        $act = "Updated profile settings / details";

#................................
try{   
  $sys_users = $db->query("SELECT `uID`, `UserName` FROM `users` WHERE `UserName` = ?  AND  `uID` <> ?",$request["uname"],$request["uid"]);     
                  }catch (unreal4u\queryException $e) {
                    print('We have captured a query exception!');
                    var_dump($e->getMessage());
                  }/////try
                  if ($db->num_rows > 0) {
                  	$msg="Username taken. Try another.";
                  	$typer = 2;
                  	header("Location: ../view/page-user.php?m=" . $msg . "&ty=".$ty);
}else{
try {
	
	$db->begin_transaction();
	if ($updatePassword){ 
		$db->query("UPDATE `users` SET `UserName`= ?,`fName`= ?,`mName`= ?,`sName`= ?,`PassWord`= ?,`eMail`= ? WHERE `uID` = ?",
				$request["uname"], $request["fn"], $request["mn"], $request["ln"], $request["pword"], $request["eMail"], $request["uid"]
		);
	}else{
		$db->query("UPDATE `users` SET `UserName`= ?,`fName`= ?,`mName`= ?,`sName`= ?,`eMail`= ? WHERE `uID` = ?",
				$request["uname"], $request["fn"], $request["mn"], $request["ln"], $request["eMail"], $request["uid"]
		);
	}

	$db->query("UPDATE `profiles` SET `address`= ?, `phone`= ? WHERE `pID` = ?",
			$request["adr"], $request["cNum"], $request["uid"]
	);
	$db->query("INSERT INTO `users_activity`(`usersID`, `actDesc`, `actDate`, `actTime`) VALUES(?,?,?,?)",$uID, $act,$when, $oras);	
	$db->end_transaction();
	$iam = $request['fn'].' '.$request['sn'];
	session_regenerate_id();
	$_SESSION['human'] = $iam;
	session_write_close();
	$msg = "Profile updated";
	$ty = 1;
	header("Location: ../view/page-user.php?m=" . $msg . "&ty=".$ty);
} catch (unreal4u\queryException $e) {
	print('We have captured a query exception! '. $e->getMessage());
	die();
}

#................................
}
}

?>





















