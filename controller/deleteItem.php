<?php
session_start();
if ($_SESSION['logged'] == '1') {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        require("../model/config.php");
        require("../model/dbmysqli.php");
        $db = new unreal4u\dbmysqli();
        $db->throwQueryExceptions = true;
        $db->query("SET SESSION sql_mode = ''");
        
        $iITem = strtoupper(trim($_GET['item']));
		
        if ($iITem) {
            try {                
             
                //add to record
                $db->begin_transaction();
                $db->query("DELETE FROM `z_purchases` WHERE `pID` = ?", $iITem);
                $db->end_transaction();
                $t=1;               
                $msg = 'Item Removed';
                header("Location: ../view/purchase_request.php?m=" . $msg . "&ty=".$t."#gdr");
           
             } catch (unreal4u\queryException $e) {
                print('We have captured a query exception!');
                var_dump($e->getMessage());
                print_r($_POST);
            }
        } else{
            $t=3;
            $msg = "No Item Selected";
            header("Location: ../view/purchase_request.php?m=" . $msg . "&ty=".$t."#gdr");
        }
    } else {
        header("Location: aDashboard.php");
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
?>
