<?php

namespace unreal4u;

/**
 * Defines the DB host
 * @var string
 */
const DB_MYSQLI_HOST = 'localhost';

/**
 * Defines the DB port
 * @var int
 */
const DB_MYSQLI_PORT = 3306;

/**
 * Defines the DB username
 * @var string
 */
const DB_MYSQLI_USER = 'root';

/**
 * Defines the DB password
 * @var string
 */
const DB_MYSQLI_PASS = '';

/**
 * Defines the DB name
 * @var string
 */
const DB_MYSQLI_NAME = 'v_liga_v';

/**
 * Defines the used database charset
 * @var string
 */
const DB_MYSQLI_CHAR = 'utf8';
