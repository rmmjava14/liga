<?php 
session_start();
$_SESSION["liga_path"]="http://localhost/liga/";
?>
<!DOCTYPE html>
<html>
<?php
include("headerL.php");
?>
<body>
<?php
include("navL.php");
?>
    <!-- Navbar End-->

    <section style="background: url('img/photogrid.jpg') center center repeat; background-size: cover;"
             class="bar background-white relative-positioned">
        <div class="container">
            <!-- Carousel Start-->
            <div class="home-carousel">
                <div class="dark-mask mask-primary"></div>
                <div class="container text-center">
                    <div class="video-area text-center" style=" background-image: url(img/home-try.jpg);">
                        <div class="video-play-btn">
                            <a href="img/video.webm" class="video_btn"><i class="fa fa-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel End-->
        </div>
    </section>
    <section id="packages" class="bar no-mb">
        <div data-animate="fadeInUp" class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h2 id="platform">Community Managed E-Commerce Platform </h2>
                    </div>
                    <!-- Platform Start-->
                    <div class="row packages">
                        <div class="col-md-4 p-2">
                            <div class="package">
                                <div class="mt-3 d-flex align-items-end justify-content-center">
                                    <img src="img/liga-g.png" alt="" style="height: 100px;">
                                </div>

                                <p class="p-3 text-muted">The LiGa Gateway is a collection of business models created by
                                    Likha at Galing that
                                    will offer a variety of products and services among members and partners. It is
                                    created by our local affiliate’s project innovator from different regions around the
                                    Philippines.</p>
                                <a href="#liga" class="btn btn-template-outlined">Learn More</a>
                            </div>
                        </div>
                        <!-- / END FIRST PLATFORM-->
                        <div class="col-md-4 p-2">
                            <div class="package">

                                <div class="mt-3 d-flex align-items-end justify-content-center">
                                    <img src="img/pinas-g.png" alt="" style="height: 100px;">
                                </div>

                                <p class="p-3 text-muted">PINAS Gateway is a combination of social enterprising and
                                    traditional business models with a unique collection of products and services online
                                    from Small and Medium Enterprise (SMEs).
                                    <br> &nbsp;
                                </p>
                                <a href="#pinas" class="btn btn-template-outlined">Learn More</a>
                            </div>
                        </div>

                        <div class="col-md-4 p-2">
                            <div class="package">

                                <div class="mt-3 d-flex align-items-end justify-content-center">
                                    <img src="img/global-g.png" alt="" style="height: 100px;">
                                </div>

                                <p class="p-3 text-muted">GLOBAL Gateway is an international shopping destination of
                                    E-commerce industry from different countries. The products and services are
                                    available online with a full pack of different items to suit your specific needs.
                                    <br> &nbsp;
                                </p>
                                <a href="#global" class="btn btn-template-outlined">Learn More</a>
                            </div>
                        </div>
                    </div>
                    <!-- Platform End-->
                </div>
            </div>
        </div>
    </section>
    <section class="bar bg-gray no-mb padding-big text-center-sm" id="liga">
        <div class="container">
            <div class="row">
                <div class="col-md-6 p-5">
                    <h2 class="text-uppercase">Likha at Galing Gateway</h2>
                    <p class="mb-small">It will provide and enhance quality assurance in all
                        stages of business cycle
                        processes. Our approach is to promote business models from traditional to E-commerce platform
                        that will facilitate a customized business solutions to each partners including risk management
                        system. A business development models that will offer various products and services catering
                        your specific needs.</p>
                    <p class="text-center"><a href="platform/platform-liga.php" class="btn btn-template-main ">Shop Now</a></p>
                </div>
                <div class="col-md-6 text-center mt-md-auto mb-md-auto"><img src="img/img_liga.png" style=" margin-top:-50px;"
                                                       alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section class="bar no-mb color-white padding-big text-md-center bg-primary" id="pinas">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center mt-md-auto mb-md-auto"><img src="img/img_pinas.png" style=" margin-top:-50px;"
                                                       alt="" class="img-fluid">
                </div>
                <div class="col-md-6 p-5">
                    <h2 class="text-uppercase">Pinas Gateway</h2>
                    <p class="mb-small" style="text-align: left;">It is created by our local affiliate’s project
                        innovator from different regions
                        around the Philippines. We promote local products crafted by local communities and different
                        sectors to popularize their inventions through E-commerce. A social enterprising initiatives
                        that will maximize a Community-Managed E-Commerce Platform wherein the merchants and customers
                        can access more opportunities to market various products and services globally. All products is
                        under the LiGa Quality Assurance Seal.</p>
                    <p>
                        <a href="platform/platform-pinas.php" class="btn btn-template-outlined-white text-center">Shop Now</a>
                    </p>

                </div>

            </div>
        </div>
    </section>

    <section class="bar bg-gray no-mb padding-big text-center-sm" id="global">
        <div class="container">
            <div class="row">
                <div class="col-md-6 p-5">
                    <h2 class="text-uppercase text-center">Global Gateway</h2>
                    <p class="mb-small">The products and services are available online with a full pack of different
                        items to suit your specific needs. It will provide an exchange of information, technologies,
                        technical and raw materials, and new markets globally. The Global Gateway is also an approach of
                        E-commerce partnership at the international level from different groups, individuals, and
                        companies who collaborate with, and to address Millennium Development Goals through business
                        enterprise.</p>
                    <p class="text-center"><a href="platform/platform-global.php" class="btn btn-template-main">Shop Now</a></p>
                </div>
                <div class="col-md-6 text-center mt-md-auto mb-md-auto"><img src="img/img_global.png" style=" margin-top:-50px;"
                                                       alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="<?=$_SESSION["liga_path"]?>membership/liga-how-to.html" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
<?php
include("footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("xscript.php");
?>
</body>
</html>