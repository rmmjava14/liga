<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

    <section class="bg-white bar">
        <div class="container">
            <div class="heading text-center">
                <img src="../img/global-g.png" alt="Liga Gateway" style="height: 100px;">
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/argentina.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">ARGENTINA </a></h4>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/usa.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">USA</a></h4>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/uk.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">UK</a></h4>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/japan.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">JAPAN</a></h4>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/hongkong.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">HONGKONG </a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/israel.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">ISRAEL</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/new-zealand.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">NEW ZEALAND</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/china.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">CHINA</a></h4>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/canada.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">CANADA </a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/thailand.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">THAILAND</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/indonesia.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">INDONESIA</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/brunei.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">BRUNIE</a></h4>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/malaysia.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">MALAYSIA</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="home-blog-post">
                        <div class="image"><img src="../img/flag/austrilia.png" alt="..." class="img-fluid">
                            <div class="overlay d-flex align-items-center justify-content-center"><a href="#"
                                                                                                     class="btn btn-template-outlined-white"><i
                                    class="fa fa-chain"> </i> Shop now</a></div>
                        </div>
                        <div class="text">
                            <h4><a href="#">AUSTRILIA</a></h4>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </div>
    </section>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>