<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

 <section class="bar background-white no-mb">
        <div class="container">
            <div class="col-md-12">
                <div class="heading text-center">
                    <img src="../img/liga-g.png" alt="Liga Gateway" style="height: 100px;">
                </div>
                <div class="row mt-5">
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/farmer-745267_1280.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">People in Development</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                </div>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item cap-blue-small"><a href="liga/specialist.html">List of Specialists</a> </li>
                                <li class="list-group-item cap-blue-small"><a href="liga/services.html">List of Services </a> </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/img-12.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">RAKIS Point Logistics</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/img-10.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">MetroYouth</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/cloud-2104829_1280.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">LiGa Cloud</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/massage-2768832_1280.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">Health Options</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/img-2.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">RAKIS Point Travel Plus</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/img-9.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">Ketsana Brothers</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/fishermen-2983615_1280.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">Catch & Fish</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/portfolio-4.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">Sweet ALO</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/sunrise-1014712_1280.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">Irgomacq Republic</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/painting-911804_1280.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">TATZ Republic</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="card">
                            <img class="card-img-top" src="../img/portfolio-6.jpg" alt="Card image cap">
                            <div class="card-body text-center">
                                <div class="liga-platform-product">
                                    <h5 class="card-title color-grey">Breathe Musk</h5>
                                    <p class="card-text author-category">By <a href="#">Ding Desembrana</a></p>
                                    <a href="#" class="btn btn-primary">Shop now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>