<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

  <div id="content">
        <div class="container">
            <div class="row bar">
                <div class="col-md-9">
                    <div class="row products products-big">
                        <div class="col-lg-4 col-md-6">
                            <div class="product">
                                <div class="image"><a href="#"><img src="../img/p10.jpg" alt=""
                                                                                      class="img-fluid image1"></a>
                                </div>
                                <div class="text">
                                    <h3 class="h5"><a href="#">Lorem ipsum dolor sit</a></h3>
                                    <!--<small><span class="text-muted">By </span><a href="#">Paulken Cabrillas</a></small>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="product">
                                <div class="image"><a href="#"><img src="../img/p11.jpg" alt=""
                                                                                      class="img-fluid image1"></a>
                                </div>
                                <div class="text">
                                    <h3 class="h5"><a href="#">Lorem ipsum dolor sit</a></h3>
                                </div>
                                <div class="ribbon-holder">
                                    <div class="ribbon new">NEW</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="product">
                                <div class="image"><a href="#"><img src="../img/p3.jpg" alt=""
                                                                                      class="img-fluid image1"></a>
                                </div>
                                <div class="text">
                                    <h3 class="h5"><a href="#">Lorem ipsum dolor sit</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="product">
                                <div class="image"><a href="#"><img src="../img/p12.jpg" alt=""
                                                                                      class="img-fluid image1"></a>
                                </div>
                                <div class="text">
                                    <h3 class="h5"><a href="#">Lorem ipsum dolor sit</a></h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="product">
                                <div class="image"><a href="#"><img src="../img/p13.jpg" alt=""
                                                                                      class="img-fluid image1"></a>
                                </div>
                                <div class="text">
                                    <h3 class="h5"><a href="#">Lorem ipsum dolor sit</a></h3>
                                </div>
                                <div class="ribbon-holder">
                                    <div class="ribbon new">NEW</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="product">
                                <div class="image"><a href="#"><img src="../img/p9.jpg" alt=""
                                                                                      class="img-fluid image1"></a>
                                </div>
                                <div class="text">
                                    <h3 class="h5"><a href=#">Lorem ipsum dolor sit</a></h3>
                                </div>
                                <div class="ribbon-holder">
                                    <div class="ribbon new">NEW</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pages">
                        <p class="loadMore text-center"><a href="#" class="btn btn-template-outlined"><i
                                class="fa fa-chevron-down"></i> Load more</a></p>
                        <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                            <ul class="pagination">
                                <li class="page-item"><a href="#" class="page-link"> <i
                                        class="fa fa-angle-double-left"></i></a></li>
                                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                                <li class="page-item"><a href="#" class="page-link">2</a></li>
                                <li class="page-item"><a href="#" class="page-link">3</a></li>
                                <li class="page-item"><a href="#" class="page-link">4</a></li>
                                <li class="page-item"><a href="#" class="page-link">5</a></li>
                                <li class="page-item"><a href="#" class="page-link"><i
                                        class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-md-3">
                    <!-- MENUS AND FILTERS-->
                    <div class="panel panel-default sidebar-menu">
                        <div class="panel-heading text-center">
                            <img src="../img/pinas-g.png" alt="" style="height: 50px;">
                        </div>
                        <div class="panel-body">
                            <ul class="nav nav-pills flex-column text-sm category-menu">
                                <li class="nav-item"><a href="#"
                                                        class="nav-link d-flex align-items-center justify-content-between"><span>Luzon </span><span
                                        class="badge badge-secondary">42</span></a>
                                    <ul class="nav nav-pills flex-column">
                                        <li class="nav-item"><a href="#" class="nav-link">NCR</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">CAR</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Ilocos Region (I)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Cagayan Valley (II)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Central Luzon(III)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">CALABARZON (IV-A)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Southwestern Tagalog</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Bicol Region (V)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Cagayan Valley (II)</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a href="#"
                                                        class="nav-link active d-flex align-items-center justify-content-between"><span>Visayas  </span><span
                                        class="badge badge-light">123</span></a>
                                    <ul class="nav nav-pills flex-column">
                                        <li class="nav-item"><a href="#" class="nav-link">Western Visayas (VI)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Central Visayas (VII)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Eastern Visayas (VIII)</a></li>

                                    </ul>
                                </li>
                                <li class="nav-item"><a href="#"
                                                        class="nav-link d-flex align-items-center justify-content-between"><span>Mindanao  </span><span
                                        class="badge badge-secondary">11</span></a>
                                    <ul class="nav nav-pills flex-column">
                                        <li class="nav-item"><a href="#" class="nav-link">Zamboanga Peninsula (IX)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Northern Mindanao (X)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">Davao Region (XI)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">SOCCSKSARGEN (XII)</a></li>
                                        <li class="nav-item"><a href="#" class="nav-link">ARMM</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>