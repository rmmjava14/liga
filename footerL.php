<footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <h4 class="h6">Information</h4>
                    <ul class="list-unstyled">
                        <li class="d-flex align-items-center"><a href="#" class="small">Delivery Information</a></li>
                        <li class="d-flex align-items-center"><a href="#" class="small">Privacy & Policy</a></li>
                        <li class="d-flex align-items-center"><a href="#" class="small">Term & Condition</a></li>
                        <li class="d-flex align-items-center"><a href="#" class="small">FQAs</a></li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <h4 class="h6">Customer Service</h4>
                    <ul class="list-unstyled">
                        <li class="d-flex align-items-center"><a href="#" class="small">Returns</a></li>
                        <li class="d-flex align-items-center"><a href="#" class="small">Site Map</a></li>
                    </ul>
                </div>
                <div class="col-lg-2">
                    <h4 class="h6">Extras</h4>
                    <ul class="list-unstyled">
                        <li class="d-flex align-items-center"><a href="#" class="small">Brands</a></li>
                        <li class="d-flex align-items-center"><a href="#" class="small">Gift Certificates</a></li>
                        <li class="d-flex align-items-center"><a href="#" class="small">Specials</a></li>
                    </ul>
                </div>
                <div class="col-lg-5">
                    <h4 class="h6">Contact</h4>
                    <ul class="list-unstyled">
                        <li class="d-flex align-items-center"><a href="#" class="small">30-A Purok I Brgy. Camp 8 Baguio
                            City
                            2600 Benguet Philippines
                        </a></li>
                        <li class="d-flex align-items-center small">Smart:&nbsp;&nbsp;<a href="#"> 09266564905</a>&nbsp;&nbsp;&nbsp;&nbsp;Globe:&nbsp;&nbsp; <a
                                href="#"> 09266564905</a></li>
                        <li class="d-flex align-items-center small">Email:&nbsp;&nbsp; <a href="#"> likha.galinggroup@gmail.com</a>
                        </li>
                        <li class="d-flex align-items-center small">Website:&nbsp;&nbsp; <a href="#"> likha-galing.com</a></li>
                        <li class="d-flex align-items-center small">Facebook:&nbsp;&nbsp; <a href="#"> likha galing</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyrights">
            <div class="container text-center">
                <p>Copyright © 2018 Likha at Galing.  All rights reserved. </p>
            </div>
        </div>
    </footer>