<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

 <section class="bar">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="heading text-center">
            <h2>Types of Payments</h2>
          </div>
          <div class="container-fluid">
            <div class="row payment-type pt-md-5">
              <div class="col-md text-center">
                <img src="../img/palawan.png" alt="" class="img-fluid">
              </div>
              <div class="col-md text-center">
                <img src="../img/ml.png" alt="" class="img-fluid">
              </div>
              <div class="col-md text-center">
                <img src="../img/cebuana.jpg" alt="" class="img-fluid">
              </div>
              <div class="col-md text-center">
                <img src="../img/gcash.jpeg" alt="" class="img-fluid">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <hr>

  <section class="bar">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-md-2">
              <img src="../img/bpi.jpg" alt="" class="img-fluid">
            </div>
            <div class="col pl-5 pr-5">
              <div class="row">
                <p class="lead">Through the <strong>Bank of Philippine Island (BPI)</strong> in any
                  branches nationwide under
                  the name of the Country Managing Director as the Official and Authorized Officer to
                  receive the payment of any transactions between the clients and the Liga Management
                  Team.</p>
              </div>

              <p><strong>Armando G. Desembrana</strong></p>
              <p>BPI Account Number: <strong>SA 0576-2209-75</strong></p>
              <p>Session Road, Baguio City Branch</p>
              <p>2600 Philippines</p>
            </div>
          </div>
          <hr>
          <div class="row text-center">
            <p class="lead">Please keep the record of any receipts of transaction payments you made from
              different remittance center as your proof of payment with us. We will acknowledge and send
              you an official receipt once we received your payment. </p>

            <p class="lead">
              We will not allowed any person to receive payments or financial transactions for any
              individuals only for the authorized name mentioned above.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>