<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

  <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="heading text-center">
                            <h2>The Connect Host Team</h2>
                        </div>

                        <p class="lead"> The development process of LiGa projects will handle through our LiGa Connect
                            Host Team. The
                            LiGa Connect Host Team will work together, plan, implement, monitor and develop the progress
                            of their projects. The “CONNECT HOST TEAM” is to be handled by the Project Coach who will
                            act as a team leader and provides coaching, mentoring among members of the connect group.
                            Through Connect Host Team, the members will help each other, facilitate mobilization,
                            organizing, capacity building, project management process, promotion and marketing.
                        </p>

                        <p class="lead">The LiGa Connect Host Team will practice a self-help learning approach within
                            the members to
                            discover strength and weaknesses for improvement of each members of the connect groups. It
                            will focus on membership development that will maximize the structure of commercial
                            undertakings to achieve social goals. </p>

                    </div>
                </div>
            </div>
        </section>
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center mb-3">
                            <h2>The Country Host Team</h2>
                        </div>

                        <div class="row">
                            <div data-animate="fadeInUp" class="col-md-3 text-center">
                                <div class="team-member">
                                    <div class="image"><a href=""><img src="../img/liga-host-2.png" alt=""
                                                                       class="img-fluid rounded-circle"></a></div>
                                    <h3><a href="">Ding G. Desembrana</a></h3>
                                    <p class="role">Country Managing Director</p>
                                    <ul class="social list-inline">
                                        <li class="list-inline-item"><a href="#" class="external facebook"><i
                                                class="fa fa-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external gplus"><i
                                                class="fa fa-google-plus"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external linkin"><i
                                                class="fa fa-linkedin"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="email"><i
                                                class="fa fa-envelope"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-md">

                                <p class="lead">DING G. DESEMBRANA is the founder and organizer of The Likha at Galing
                                    E-Commerce Project Management Services or LiGa, which piloted on March 8, 2008.
                                    Deeply
                                    moved by issues of poverty and hunger, Ding created the Community-Managed E-Commerce
                                    Platform (CMECP) to help out underprivileged individuals, families, communities and
                                    small businesses gain financial freedom through e-commerce and trade industry. He
                                    pledged to devote his time, experience, and management capability during the
                                    formulation
                                    stage and to find necessary resources to establish the roots of E-commerce industry.
                                </p>
                                <p class="lead">
                                    Ding G. Desembrana has been a social entrepreneur since 2004 and has been sharing
                                    his
                                    knowledge, expertise, and experiences in business transactions for a social cause.
                                    All
                                    these gave him enough motivation to build the LiGa platform. The LiGa became
                                    operative
                                    to provide social services, carry out extensive research, explore strategies and
                                    synergies, and bring together commercial and social activities under a combine
                                    platform
                                    for a social cause. It aims to evaluate, synthesize and magnetize self-sustaining
                                    business models to stimulate entrepreneurial activities with social benefits
                                    reaching
                                    all various sectors at all levels.
                                </p>

                                <p class="lead">
                                    He has worked for more than 30 years in different International Non-Government
                                    Organizations as a Professional Trainer and Technical Consultant in various fields
                                    of
                                    programs and community development. He also became a Technical Evaluator of
                                    Child-led
                                    programs, and has acquired expertise in Program/ Project Management & Development,
                                    Community Organizing, Networking & Advocacy, Research & Education Designing,
                                    Implementation & Analysis, Training Development & Delivery, Resource Mobilization,
                                    Program Mentoring and Coaching.
                                </p>

                            </div>

                        </div>
                        <hr class="mb-5">
                        <div class="row">
                            <div data-animate="fadeInUp" class="col-md-3 text-center">
                                <div class="team-member">
                                    <div class="image"><a href=""><img src="../img/liga-host-1.png" alt=""
                                                                       class="img-fluid rounded-circle"></a></div>
                                    <h3><a href="">Louis F. Guillem</a></h3>
                                    <p class="role">Deputy Managing Director</p>
                                    <ul class="social list-inline">
                                        <li class="list-inline-item"><a href="#" class="external facebook"><i
                                                class="fa fa-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external gplus"><i
                                                class="fa fa-google-plus"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external linkin"><i
                                                class="fa fa-linkedin"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="email"><i
                                                class="fa fa-envelope"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-md">

                                <p class="lead">MR. LOUIS F. GUILLEM, a Nurse by profession, has passion for volunteer
                                    work, sharing innovating knowledge and skills with community based organization
                                    geographically isolated areas and marginalized sector. He has been working as a
                                    Development and Health Specialist for more than 20 years in RH Disaster Response,
                                    Good Governance, HIV AIDS & Family Planning, ECCD, Education and Child Protection.
                                </p>
                                <p class="lead">
                                    He has been into several local and international development organization. Assigned
                                    in several region and provinces. He has managed various level of positions on
                                    project engagement with different complexities and at times in areas with high
                                    security level. He is presently engaged in Education Grant Project with an
                                    International Development Organization as Professional Development Specialist.
                                </p>
                            </div>

                        </div>
                        <hr class="mb-5">
                        <div class="row">
                            <div data-animate="fadeInUp" class="col-md-3 text-center">
                                <div class="team-member">
                                    <div class="image"><a href=""><img src="../img/liga-host-3.png" alt=""
                                                                       class="img-fluid rounded-circle"></a></div>
                                    <h3><a href="">Julie Valerio Passi</a></h3>
                                    <p class="role">Project Managing Director</p>
                                    <ul class="social list-inline">
                                        <li class="list-inline-item"><a href="#" class="external facebook"><i
                                                class="fa fa-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external gplus"><i
                                                class="fa fa-google-plus"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external linkin"><i
                                                class="fa fa-linkedin"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="email"><i
                                                class="fa fa-envelope"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-md">

                                <p class="lead">MS. JULIE VALERIO-PASSI, is a registered nurse , who finds the meaning
                                    of truly serving the people in the midst of diverse surroundings among the poor,
                                    marginalized and underprivileged communities. She has been working as a community
                                    health nurse for more than 35 years advocating health and human rights, forging
                                    partnership with different stakeholders in promoting better health and welfare for
                                    the people.
                                </p>
                                <p class="lead">
                                    This mission has been her torch to carry on development for health . She has been
                                    into development work engaged in national and international development agencies,
                                    private and non-profit organizations. Her involvement mostly focused on providing
                                    technical skills, provision of service, project development and community
                                    participation. She is a skilled trainer- facilitator, community organizer, and has
                                    expertise on Program/ Project Management & Development, Community Organizing,
                                    Networking & Advocacy, Implementation & Analysis, Training Development & Delivery,
                                    Resource Mobilization, Program Mentoring and Coaching.
                                </p>

                                <p class="lead">
                                    She is presently a doing consultancy work in different International Non-Government
                                    Organizations as a Professional Trainer, Technical Consultant on various fields of
                                    programs and community development.
                                </p>
                            </div>

                        </div>
                        <hr class="mb-5">
                        <div class="row">
                            <div data-animate="fadeInUp" class="col-md-3 text-center">
                                <div class="team-member">
                                    <div class="image"><a href=""><img src="../img/liga-host-4.png" alt=""
                                                                       class="img-fluid rounded-circle"></a></div>
                                    <h3><a href="">Jane Balares</a></h3>
                                    <p class="role">Finance Director</p>
                                    <ul class="social list-inline">
                                        <li class="list-inline-item"><a href="#" class="external facebook"><i
                                                class="fa fa-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external gplus"><i
                                                class="fa fa-google-plus"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external linkin"><i
                                                class="fa fa-linkedin"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="email"><i
                                                class="fa fa-envelope"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-md">

                                <p class="lead">JANE SERVIDAD-BALARES, is a passionate and highly motivated person in
                                    pursuing community development work. By profession, she is a graduate of B.S.C.
                                    major in Accounting. She can integrate her knowledge in accountancy in Peoples
                                    Organization’s economic activities like installation of internal control system,
                                    bookkeeping and financial management. She provides technical assistance in project
                                    development to Community-Based Organizations to implement sustainable livelihood.
                                </p>
                                <p class="lead">
                                    Her 25 years direct involvement in implementing projects/programs of different local
                                    and International NonGovernment Organizations gave her vast experience in
                                    supervising, monitoring and evaluation of services provided to children, youth,
                                    women, farmers and other sectors.
                                </p>

                                <p class="lead">
                                    She is an advocate of environmental protection, women’s rights, gender equality and
                                    promotes good governance in leadership and team building. She has the ability to
                                    coach/mentor and transfer skills, knowledge and technology to target stakeholders to
                                    organize themselves, influence and participate in developmental projects.
                                </p>

                                <p class="lead">
                                    Currently, she is providing services in different government offices and
                                    Non-Government Organizations as Technical Consultant, Trainer and Documenter. She
                                    also works as a Financial Advisor that helps her clients plan for their short and
                                    long-term financial goals.


                                </p>
                            </div>

                        </div>
                        <hr class="mb-5">
                        <div class="row">
                            <div data-animate="fadeInUp" class="col-md-3 text-center">
                                <div class="team-member">
                                    <div class="image"><a href=""><img src="../img/liga-host-5.png" alt=""
                                                                       class="img-fluid rounded-circle"></a></div>
                                    <h3><a href="">Lovely De Guzman</a></h3>
                                    <p class="role">Communication Specialist</p>
                                    <ul class="social list-inline">
                                        <li class="list-inline-item"><a href="#" class="external facebook"><i
                                                class="fa fa-facebook"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external gplus"><i
                                                class="fa fa-google-plus"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="external linkin"><i
                                                class="fa fa-linkedin"></i></a></li>
                                        <li class="list-inline-item"><a href="#" class="email"><i
                                                class="fa fa-envelope"></i></a></li>
                                    </ul>

                                </div>
                            </div>
                            <div class="col-md">

                                <p class="lead">LOVELY DE GUZMAN is a homebased freelancer and has worked on various
                                    projects that involve web research, blogging, ghostwriting, data transcription and
                                    data entry. She began her freelancing career early this year and found a huge
                                    potential in it considering that many people are now opting for homebased jobs,
                                    including stay-at-home moms like her. She is continually learning and honing her
                                    skills in social media marketing, graphics designing and many more.
                                </p>
                                <p class="lead">
                                    Prior to her freelancing career, she worked as a Legal Editor from 2013 to 2015 in a
                                    knowledge processing outsourcing (KPO) company where her tasks include web
                                    researching and writing nonlegalese summaries of international tax developments. She
                                    also worked as a Legal Secretary from in a law office and Freelance Training
                                    Documenter whose tasks include documenting workshops/ trainings conducted by
                                    different NGOs across the Philippines, particularly Plan International and Christian
                                    Children’s Fund.
                                </p>

                                <p class="lead">
                                    Aside from writing, she also loves painting portraits and landscapes using oil
                                    pastels. She dreams of exhibiting her artworks someday.
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.html" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>