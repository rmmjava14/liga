<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

    <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>Our Core Values </h2>
                        </div>

                        <div class="text-center">
                            <p class="lead">Our commitment is to provide the standard service in terms of quality and
                                integrity of our clients. Our core values are: </p>

                            <p class="lead">1. God Wisdom </p>

                            <p class="lead">2. Obedience & Loyalty </p>

                            <p class="lead">3. Development </p>

                            <p class="lead">4. Honesty </p>

                            <p class="lead">5. Achievement-oriented </p>

                            <p class="lead">6. Partnership </p>

                            <p class="lead">7. Integrity </p>
                        </div>

                        <hr>

                        <div class="container">
                            <div class="col-md-12">
                                <p class="lead">Our success is reliant on self-help learnings, self-help empowering, and
                                    collective actions through the achievement of an exceptionally high level of team
                                    spirit and local wisdom from among members as we move along the path guided with the
                                    following values: </p>

                                <ul class="ul-icons list-unstyled">
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Application of the highest ethical standards of LiGa, both in its commercial operations and in various markets in which it operates. </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled ">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Strictly adhere to Philippine Government policy in terms of trade and commerce undertakings particularly the E-commerce platform . </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Continue to seek ways of improving efficiency and quality of services in all of its operations. </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled mt-2">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Provide social equity and assistance to the community of youth and women with innovative projects to become resilience sectors with a new mindset from employment to virtual entrepreneurship. </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Maintain the highest level of integrity in all its dealings with client. </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Establish to value clients and staff development programs, privileges and benefits that will be an essential component in achieving goals and activities. </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="icon-filled">
                                                <i class="fa fa-check"></i>
                                            </div>
                                            <div class="col">
                                                <span>Maintain such qualities as culturally-sensitive and resourcefulness of entrepreneurial spirit and an appreciation of commercial imperatives.</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                                <p class="lead">Our primary operations focus on the Philippine territory while LiGa will
                                    take advantage of opportunistic markets and investors wherever they arise
                                    globally. </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.html" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>