<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

   <div id="content">
        <div class="container">
            <section class="bar pb-0">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="heading text-center">
                            <h2>What is LiGa</h2>
                        </div>
                        <p class="lead">&nbsp; &nbsp; &nbsp; &nbsp; The Likha at Galing E-commerce Project Management
                            Services or LiGa is a community of project innovators, entrepreneurs, individuals, groups,
                            and different sectors, working together to facilitate the establishment of e-Commerce and
                            project development platform through digital industry. The LiGa is an outsourcing of virtual
                            project-based platform facilitating the development process of each project mainly through
                            the internet. It will promote collaboration of concepts and project innovations with a
                            common goal among visionaries who have big ideas and who need crucial startup support to
                            turn their ideas into action.

                        </p>

                        <p class="lead">&nbsp; &nbsp; &nbsp; &nbsp; The LiGa will provide capacity building activities
                            among members through Pathfinder: Accompaniment Platform Project Management System or APPMS.
                            It will serve as a support mechanism for innovation, motivation and verve to a creation of
                            different projects using different legal forms, both in terms of cost and effective
                            regulation. It will represent the future of visionaries and members in a global knowledge
                            economy. Our members are at the cutting-edge of their fields and expertise who we
                            collaborate with, to realize opportunities for social value creation, and represent the most
                            forward-thinking innovators.</p>

                        <p class="lead">&nbsp; &nbsp; &nbsp; &nbsp; The Likha at Galing E-Commerce Project Management
                            Services (LiGa) is registered with Philippine DTI Certificate No. 05332255. It will help
                            facilitate share, consolidate and develop E-Commerce venture and development of LiGa
                            projects. Thus, the LiGa will design the entry and saturation strategy, build critical mass,
                            implement the Growth, Expansion and Replication (GEAR) and exit strategies to generate
                            intrinsic values of "Pathfinder APPMS” both commercially and socially.</p>
                    </div>
                </div>
            </section>

            <section class="bar mt-0">
                <div class="row">
                    <div class="col-md-6 text-center composition">
                        <div class="card composition__photo composition__photo--p1">
                            <div class="card-body shadow">
                                <div class="heading text-center">
                                    <h2>Our Vision</h2>
                                </div>
                                <p> We envision project innovators/inventors, entrepreneurs, individuals and groups from various
                                    sectors are fully equipped, connected, and united into a sustainable e-Commerce <br>
                                    platform. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="card composition__photo composition__photo--p2">
                            <div class="card-body shadow">
                                <div class="heading text-center">
                                    <h2>Our Mission</h2>
                                </div>
                                <p> We aim full participation of each members by establishing ECommerce Platform and Sustainable
                                    Projects in the context of social enterprise maximizing "Pathfinder Accompaniment Platform
                                    Project Management System”. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        <section class="bar no-mb bg-gray">
            <div class="container">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h2>Our Goal</h2>
                    </div>

                    <ul class="ul-icons list-unstyled">
                        <li>
                            <div class="row">
                                <div class="icon-filled mt-2">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col">
                                    <span>Create an opportunity for individuals to become a project owner as a career pathway to make
                                    a difference in the e-commerce and trade industry. Assist them through part ownership in
                                    practical projects to create social wealth enterprises.</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="icon-filled ">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col">
                                    <span>Leverage available resources and potentials of Pathfinder Lab for project development processes that has direct impact in the general public. </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="icon-filled mt-2">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col">
                                    <span>Develop and provide capacity building activities both online and offline training courses, workshops, and seminars related to the e-commerce platform and project management processes.</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="icon-filled mt-2">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col">
                                    <span>Provide social equity and assistance to the community of youth and women with innovative projects to become resilience sectors with a new mindset from employment to virtual entrepreneurship. </span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="icon-filled">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="col">
                                    <span>Generate personal financial security from a closely held project enterprise.</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="bar">
            <div class="container text-center">
                <div class="heading text-center">
                    <h2>Our Call</h2>
                </div>
                <i class="fa fa-quote-left quote-b" style="vertical-align: top"></i>&nbsp; <span
                    style="font-size: 1.4rem">Make a Business to Change the Lives of People and a Resilience
                Communities</span> &nbsp;<i class="fa fa-quote-right quote-b" style="vertical-align: top"></i>
            </div>
        </section>
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>FOUR BROAD RECIPIENTS </h2>
                        </div>
                        <div class="row d-flex align-items-stretch same-height">
                            <div class="col-md-3">
                                <div class="box-simple box-white same-height">
                                    <div class="icon"><i class="fa fa-users color-blue"></i></div>
                                    <h4>Children and Youth</h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box-simple box-white same-height">
                                    <div class="icon"><i class="fa fa-user color-blue"></i></div>
                                    <h4>Gender</h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box-simple box-white same-height">
                                    <div class="icon"><i class="fa fa-user-secret color-blue"></i></div>
                                    <h4>People with Special Needs</h4>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="box-simple box-white same-height">
                                    <div class="icon"><i class="fa fa-globe color-blue"></i></div>
                                    <h4>Disaster Risk Reduction and Climate Change Adaptation</h4>
                                </div>
                            </div>
                        </div>
                        <!--<ul class="ul-icons list-unstyled text-center">-->
                        <!--<li>-->
                        <!--<span><i class="fa fa-check quote-b"></i> Children and Youth </span>-->
                        <!--</li>-->
                        <!--<li>-->
                        <!--<span><i class="fa fa-check quote-b"></i> Gender</span>-->
                        <!--</li>-->
                        <!--<li>-->
                        <!--<span><i class="fa fa-check quote-b"></i> People with Special Needs</span>-->
                        <!--</li>-->
                        <!--<li>-->
                        <!--<span><i class="fa fa-check quote-b"></i> Disaster Risk Reduction and Climate Change Adaptation  </span>-->
                        <!--</li>-->
                        <!--</ul>-->

                    </div>
                </div>
            </div>
        </section>


    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.php" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>