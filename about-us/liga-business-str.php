<?php 
session_start();
?>
<!DOCTYPE html>
<html>
<?php
include("../headerL.php");
?>
<body>
<?php
include("../navL.php");
?>    <!-- Navbar End-->

  <div id="content">
        <section class="bar mb-0">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading text-center">
                            <h2>Our Business Strengths</h2>
                        </div>

                        <p class="lead">1. Generates indefinite number of opportunities as a means of an alternative or additional source of income.</p>

                        <p class="lead">2. Provides generous and lifetime benefits and privileges. </p>

                        <p class="lead">3. Channels a fraction of its income to provide support vulnerable groups such as children and youth, women, people with special needs, Disaster Risk Reduction and Climate Change Adaptation programs and activities. </p>

                        <p class="lead">4. Acknowledges God as the Great Provider. Most of its business income or otherwise known as "tithe" is returned to "The One from Whom All Blessings Flow." </p>

                        <p class="lead">5. Cultivates and develops the spirit of entrepreneurial potential among partners, clients and staff. </p>

                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- GET IT-->
    <div class="get-it">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-center p-3">
                    <h3>How to become a member of liga?</h3>
                </div>
                <div class="col-lg-4 text-center p-3"><a href="../membership/liga-how-to.html" class="btn btn-template-outlined-white">Get
                    Started</a></div>
            </div>
        </div>
    </div>
    <!-- FOOTER -->
    <?php
include("../footerL.php");
?>
</div>
<!-- Javascript files-->
<?php
include("../xscript.php");
?>
</body>
</html>